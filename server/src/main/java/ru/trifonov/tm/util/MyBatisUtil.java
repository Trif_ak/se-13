package ru.trifonov.tm.util;

import lombok.experimental.UtilityClass;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.service.PropertyService;

import javax.sql.DataSource;

@UtilityClass
public class MyBatisUtil {
//    PropertyService propertyService = new PropertyService();
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = "root";
//        System.out.println(user);
        @Nullable final String password = "root";
//        System.out.println(password);
        @Nullable final String url = "jdbc:mysql://127.0.0.1:3306/tm";
//        System.out.println(url);
        @Nullable final String driver = "com.mysql.jdbc.Driver";
//        System.out.println(driver);
        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
