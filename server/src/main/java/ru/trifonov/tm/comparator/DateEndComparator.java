package ru.trifonov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.ComparableEntity;

import java.util.Comparator;

public final class DateEndComparator implements Comparator<ComparableEntity> {
    @Override
    public int compare(@NotNull final ComparableEntity o1, @NotNull final ComparableEntity o2) {
        return o1.getBeginDate().compareTo(o2.getBeginDate());
    }
}
