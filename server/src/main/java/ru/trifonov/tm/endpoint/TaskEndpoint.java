package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull final Task task
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().persist(task);
    }

    @Override
    @WebMethod
    public void initTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().insert(name, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception, ParseException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().update(name, id, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public Task getTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().get(id, userId);
    }

    @Override
    @WebMethod
    public List<Task> getAllTaskOfProject (
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().getOfUser(projectId, userId);
    }

    @Override
    @WebMethod
    public void deleteTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().deleteOfProject(projectId, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().deleteOfUser(userId);
    }

    @Override
    @WebMethod
    public List<Task> sortByTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().sortBy(projectId, userId, comparatorName);
    }

    @Override
    @WebMethod
    public List<Task> getTaskByPartString(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().getByPartString(userId, projectId, partString);
    }

    @Override
    @WebMethod
    public List<Task> getAllTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getTaskService().getAll();
    }
}
