package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    public ProjectEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "project", partName = "project") @NotNull final Project project
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().persist(project);
    }

    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().insert(name, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().update(name, id, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public Project getProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().get(id, userId);
    }

    @Override
    @WebMethod
    public List<Project> getAllProjectOfUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().getOfUser(userId);
    }

    @Override
    @WebMethod
    public void deleteProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().deleteAll(userId);
    }

    @Override
    @WebMethod
    public List<Project> sortByProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().sortBy(userId, comparatorName);
    }

    @Override
    @WebMethod
    public List<Project> getProjectByPartString(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().getByPartString(userId, partString);
    }

    @Override
    @WebMethod
    public List<Project> getAllProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getProjectService().getAll();
    }
}