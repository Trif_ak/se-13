package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.IdUtil;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "user")
public final class User implements Serializable {
    @NotNull
    private String id = IdUtil.getUUID();
    @NotNull
    private String login = "";
    @NotNull
    private String passwordHash = "";
    private RoleType roleType;

    public User(
            @NotNull String login,
            @NotNull String passwordHash, RoleType roleType
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", roleType=" + roleType +
                '}';
    }
}
