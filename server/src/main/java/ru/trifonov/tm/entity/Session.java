package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.IdUtil;


@Getter
@Setter
@NoArgsConstructor
public final class Session implements Cloneable {
    @NotNull
    private String id = IdUtil.getUUID();
    @NotNull
    private String userId;
    @NotNull
    private Long timestamp = System.currentTimeMillis();
    @Nullable
    private String signature;
    @Nullable
    private RoleType role;

    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }
}