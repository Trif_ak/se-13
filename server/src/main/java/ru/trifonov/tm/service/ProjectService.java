package ru.trifonov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.api.service.IProjectService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.util.IdUtil;
import ru.trifonov.tm.util.MyBatisUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends ComparatorService implements IProjectService {
    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.persist(project);
            sqlSession.commit();
        }
    }

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String userId,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.merge(project);
            sqlSession.commit();
        }
    }

    @Override
    public void update(
            @Nullable final String name, @Nullable final String id,
            @Nullable final String userId, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, id, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        merge(project);
    }

    @Override
    @NotNull public List<Project> getAll() {
        @Nullable List<Project> projects;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projects = projectRepository.getAll();
            if (projects.isEmpty()) throw new NullPointerException("Projects no found.");
        }
        return projects;
    }

    @Override
    @NotNull public List<Project> getOfUser(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable List<Project> projects;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projects = projectRepository.getOfUser(userId);
            if (projects.isEmpty()) throw new NullPointerException("Projects no found.");
        }
        return projects;
    }

    @Override
    public List<Project> getByPartString(@Nullable final String userId, @Nullable final String partString) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable final List<Project> projects = getOfUser(userId);
        if (projects.isEmpty()) throw new NullPointerException("Projects not found.");

        @Nullable final List<Project> output = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (project.getName().contains(partString) || project.getDescription().contains(partString)) {
                output.add(project);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Projects not found.");
        return output;
    }

    @Override
    public Project get(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable Project project;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            project = projectRepository.get(id, userId);
            if (project == null) throw new NullPointerException("Project no found.");
        }
        return project;
    }

    @Override
    public void deleteAll(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.deleteAll(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void delete(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.delete(id, userId);
            sqlSession.commit();
        }
    }

    @Override
    public List<Project> sortBy(@Nullable final String userId, @Nullable final String comparatorName){
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Project> projects = getOfUser(userId);
        if (projects.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }
}