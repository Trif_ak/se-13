package ru.trifonov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;
import ru.trifonov.tm.util.MyBatisUtil;

import java.util.List;

public final class UserService extends ComparatorService implements IUserService {
    @Override
    public void persist(@Nullable final User user) {
        if (user == null) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.persist(user);
            sqlSession.commit();
        }
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String password, @Nullable final RoleType roleType
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");

        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(id, login, password, roleType);
        }
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.delete(id);
        }
    }

    @Override
    @NotNull public List<User> getAll() {
        @Nullable List<User> users;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            users = userRepository.getAll();
            System.out.println(users);
            if (users.isEmpty()) throw new NullPointerException("Nothing found");
        }
        return users;
    }

    @Override
    @NotNull public User get(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable User user;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user = userRepository.get(id);
            if (user == null) throw new NullPointerException("Nothing found");
        }
        return user;
    }

    @Override
    public void changePassword(@Nullable final String userId, @Nullable final String newPassword) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (newPassword == null || newPassword.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final String newPasswordHash = HashUtil.md5(newPassword);
            userRepository.changePassword(userId, newPasswordHash);
        }
    }

    @Override
    public void registrationUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(login, HashUtil.md5(password), RoleType.REGULAR_USER);
        persist(user);
    }

    @Override
    public void registrationAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(login, HashUtil.md5(password), RoleType.ADMIN);
        persist(user);
    }

    @Override
    public void addUser() throws Exception {
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User admin = new User();
            admin.setLogin("admin");
            if (getByLogin(admin.getLogin())) throw new Exception("Login already exist.");
            admin.setPasswordHash(HashUtil.md5("admin"));
            admin.setRoleType(RoleType.ADMIN);
            userRepository.persist(admin);
            sqlSession.commit();
        }

        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final User user = new User();
            user.setLogin("user");
            if (getByLogin(user.getLogin())) throw new Exception("Login already exist.");
            user.setPasswordHash(HashUtil.md5("user"));
            user.setRoleType(RoleType.REGULAR_USER);
            userRepository.persist(user);
            sqlSession.commit();
        }
    }

    @Override
    @NotNull public User existsUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable User user;

        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final String passwordHash = HashUtil.md5(password);
            user = userRepository.existsUser(login, passwordHash);

            if (user == null) throw new NullPointerException("User not exist");
        }
            return user;
    }

    @Override
    public boolean getByLogin(@NotNull final String login) {
        @Nullable String loginGet = "";
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            loginGet = userRepository.getByLogin(login);
            if (loginGet == null || loginGet.trim().isEmpty()) return false;
        }
        return login.equals(loginGet);
    }

    @Override
    public void load(@Nullable final List<User> users
    ) {
        if (users == null) return;
        for (@NotNull final User user : users) {
            persist(user);
        }
    }
}