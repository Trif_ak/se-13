package ru.trifonov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.api.service.ITaskService;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.util.IdUtil;
import ru.trifonov.tm.util.MyBatisUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends ComparatorService implements ITaskService {
    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.persist(task);
            sqlSession.commit();
        }
    }

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String projectId,
            @Nullable final String userId, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, IdUtil.getUUID(), projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.merge(task);
            sqlSession.commit();
        }
    }

    @Override
    public void update(
            @Nullable final String name, @Nullable final String id,
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, id, projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        merge(task);
    }

    @Override
    @NotNull public List<Task> getAll() {
        @Nullable List<Task> tasks;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            tasks = taskRepository.getAll();
            if (tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        }
        return tasks;
    }

    @Override
    @NotNull public List<Task> getOfUser(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable List<Task> tasks;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            tasks = taskRepository.getOfUser(projectId, userId);
            if (tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        }
        return tasks;
    }

    @Override
    @NotNull public List<Task> getByPartString(
            @Nullable final String userId, @Nullable final String projectId,
            @Nullable final String partString
    ) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable final List<Task> tasks = getOfUser(projectId, userId);
        if (tasks.isEmpty()) throw new NullPointerException("Tasks not found");

        @Nullable final List<Task> output = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (task.getName().contains(partString) || task.getDescription().contains(partString)) {
                output.add(task);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Tasks not found.");
        return output;
    }

    @Override
    @NotNull public Task get(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable Task task;
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            task = taskRepository.get(id, userId);
            if (task == null) throw new NullPointerException("Task not found.");
        }
        return task;
    }

    @Override
    public void deleteOfUser(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.deleteOfUser(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void deleteOfProject(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.deleteOfProject(projectId, userId);
            sqlSession.commit();
        }
    }

    @Override
    public void delete(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        try(@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) throw new NullPointerException("Something wrong with connection");
            ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.delete(id, userId);
            sqlSession.commit();
        }
    }

    @Override
    @NotNull public List<Task> sortBy(
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String comparatorName
    ) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Task> tasks = getOfUser(projectId, userId);
        if (tasks.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        for (@NotNull final Task task : tasks) {
            persist(task);
        }
    }
}