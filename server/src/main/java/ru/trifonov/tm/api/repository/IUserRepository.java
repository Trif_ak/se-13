package ru.trifonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserRepository {
    @Insert("INSERT INTO user (id, login, password_hash, role) VALUES (#{id}, #{login}, #{passwordHash}, #{roleType})")
    void persist(@NotNull User user);

    @Update("UPDATE user SET login = #{login}, password_hash = #{passwordHash}, role = #{roleType} " +
            "WHERE id = #{id}")
    void update(
            @Param("id") @NotNull String id, @Param("login") @NotNull String login,
            @Param("passwordHash") @NotNull String passwordHash, @Param("roleType") @NotNull RoleType roleType
    );

    @Select("SELECT * FROM user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable List<User> getAll();

    @Select("SELECT login FROM user WHERE login = #{login}")
    @Nullable String getByLogin(@Param("login") @NotNull String login);

    @Select("SELECT * FROM user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable User get(@Param("id") @NotNull String id);

    @Delete("DELETE FROM user WHERE id = #{id}")
    void delete(@Param("id") @NotNull String id);

    @Select("SELECT * FROM user WHERE login = #{login} AND password_hash = #{passwordHash}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable User existsUser(@Param("login") @NotNull String login, @Param("passwordHash") @NotNull String passwordHash);

    @Update("UPDATE user SET password_hash = #{newPasswordHash} WHERE id = #{id}")
    void changePassword(@Param("id") @NotNull String id, @Param("newPasswordHash") @NotNull String newPasswordHash);
}
