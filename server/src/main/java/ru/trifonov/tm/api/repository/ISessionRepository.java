package ru.trifonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    @Insert("INSERT INTO session (id, user_id, timestamp, signature, role) " +
            "VALUES (#{id}, #{userId}, #{timestamp}, #{signature}, #{role})")
    void persist(@NotNull Session session);

    @Delete("DELETE FROM session WHERE id = #{id}")
    void delete(@NotNull String id);

    @Select("SELECT * FROM session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "role", column = "role")

    })
    @Nullable List<Session> getByUserId(@NotNull String userId);

    @Select("SELECT * FROM session")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "role", column = "role")

    })
    @Nullable List<Session> getAll();
}
