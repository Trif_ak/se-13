package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.enumerate.RoleType;

import javax.validation.constraints.Null;
import java.util.List;

public interface ISessionService {
    Session sign(@Nullable Session session);
    void validateAdmin(@Nullable Session session) throws Exception;
    void validate(@Nullable Session session) throws Exception;
    Session openSession(@Nullable String login, @Nullable String password) throws Exception;
    void closeSession(@Nullable Session session) throws Exception;
    void persist(@Nullable Session session) throws Exception;
    List<Session> getByUserId(@NotNull Session session) throws Exception;
    List<Session> getAll() throws Exception;
    void load(@Nullable List<Session> sessions) throws Exception;
}
