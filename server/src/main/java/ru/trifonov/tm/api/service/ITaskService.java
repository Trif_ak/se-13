package ru.trifonov.tm.api.service;

import ru.trifonov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task) throws Exception;

    void merge(@Nullable Task task);

    void insert(@Nullable String name, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void update(@Nullable String name, @Nullable String id, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Task get(@Nullable String id, @Nullable String userId) throws Exception;
    List<Task> getOfUser(@Nullable String projectId, @Nullable String userId) throws Exception;
    void delete(@Nullable String id, @Nullable String userId) throws Exception;
    void deleteOfProject(@Nullable String projectId, @Nullable String userId) throws Exception;
    void deleteOfUser(@Nullable String userId) throws Exception;
    List<Task> sortBy(@Nullable String projectId, @Nullable String userId, @Nullable String comparatorName) throws Exception;
    List<Task> getByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString) throws Exception;
    List<Task> getAll() throws Exception;
    void load(@Nullable List<Task> tasks) throws Exception;
}
