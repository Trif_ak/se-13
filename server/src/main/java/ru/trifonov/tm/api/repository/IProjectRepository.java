package ru.trifonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.*;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    @Insert("INSERT INTO project (id, user_id, name, description, status, begin_date, end_date, create_date) " +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{beginDate}, #{endDate}, #{createDate})")
    void persist(@NotNull Project project);

    @Update("UPDATE user SET name = #{name}, description = #{description}, status = #{status}" +
            "begin_date = #{beginDate}, end_date = #{endDate}, create_date = #{createDate}" +
            "WHERE id = #{id} AND user_id = #{userId}")
    void merge(@NotNull Project project);

    @Select("SELECT * FROM project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable List<Project> getAll();

    @Select("SELECT * FROM project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable List<Project> getOfUser(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable Project get(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId);

    @Delete("DELETE FROM project WHERE user_id = #{userId}")
    void deleteAll(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM project WHERE id = #{id} AND user_id = #{userId}")
    void delete(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId);
}
