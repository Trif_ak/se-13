package ru.trifonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    @Insert("INSERT INTO task (id, project_id, user_id, name, description, status, begin_date, end_date, create_date) " +
            "VALUES (#{id}, #{projectId}, #{userId}, #{name}, #{description}, #{status}, #{beginDate}, #{endDate}, #{createDate})")
    void persist(@NotNull Task task);

    @Update("UPDATE task SET name = #{name}, description = #{description}, status = #{status}, " +
            "begin_date = #{beginDate}, end_date = #{endDate}, create_date = #{createDate} " +
            "WHERE id = #{id} AND project_id = #{projectId} AND user_id = #{userId}")
    void merge(@NotNull Task task);

    @Select("SELECT * FROM task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable List<Task> getAll();

    @Select("SELECT * FROM task WHERE project_id = #{projectId} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable List<Task> getOfUser(@Param("projectId") @NotNull String projectId, @Param("userId") @NotNull String userId);

    @Select("SELECT * FROM task WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "beginDate", column = "begin_date"),
            @Result(property = "endDate", column = "end_date"),
            @Result(property = "createDate", column = "create_date")
    })
    @Nullable Task get(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId);

    @Delete("DELETE FROM task WHERE user_id = #{userId}")
    void deleteOfUser(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM task WHERE project_id = #{projectId} AND user_id = #{userId}")
    void deleteOfProject(@Param("projectId") @NotNull String projectId, @Param("userId") @NotNull String userId);

    @Delete("DELETE FROM task WHERE id = #{id} AND user_id = #{userId}")
    void delete(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId);
}
