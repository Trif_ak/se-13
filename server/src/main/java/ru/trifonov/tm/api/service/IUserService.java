package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserService {
    void persist(@Nullable User user) throws Exception;
    User existsUser(@Nullable String login, @Nullable String password) throws Exception;
    void registrationUser(@Nullable String login, @Nullable String password) throws Exception;
    void registrationAdmin(@Nullable String login, @Nullable String password) throws Exception;
    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws Exception;
    List<User> getAll() throws Exception;
    User get(@Nullable String id) throws Exception;
    void delete(@Nullable String id) throws Exception;

    @NotNull boolean getByLogin(@NotNull String login);

    void load(@Nullable List<User> users) throws Exception;
    void addUser() throws Exception;
    void changePassword(@Nullable String userId, @Nullable String newPassword) throws Exception;
}
