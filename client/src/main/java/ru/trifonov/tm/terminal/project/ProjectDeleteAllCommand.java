package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ProjectDeleteAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL PROJECTS]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getTaskEndpoint().deleteAllTaskOfUser(currentSession);
        bootstrap.getProjectEndpoint().deleteAllProject(currentSession);
        System.out.println("[OK]");
    }
}
