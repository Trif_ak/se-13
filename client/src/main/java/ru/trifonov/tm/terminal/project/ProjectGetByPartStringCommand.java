package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Project;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;


import java.util.List;

public final class ProjectGetByPartStringCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "project-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter part of title or description");
        @Nullable final String partString = bootstrap.getTerminalService().getInCommand();
        @NotNull final List<Project> projects = bootstrap.getProjectEndpoint().getProjectByPartString(currentSession, partString);
        for (@NotNull final Project project : projects) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
