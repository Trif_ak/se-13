package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.Task;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class TaskGetCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET TASK]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        @NotNull final Task task = bootstrap.getTaskEndpoint().getTask(currentSession, id);
        System.out.print("  NAME TASK " + task.getName());
        System.out.print("  DESCRIPTION TASK " + task.getDescription());
        System.out.println("  ID TASK " + task.getId());
        System.out.println("[OK]");
    }
}
