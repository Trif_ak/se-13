package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserChangePasswordCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-changePas";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD CHANGE]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter new password");
        @Nullable final String newPassword = bootstrap.getTerminalService().getInCommand();
        bootstrap.getUserEndpoint().changePassword(currentSession, newPassword);
        System.out.println("[OK]");
    }
}