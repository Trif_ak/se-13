package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainDeserializable extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-des";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": deserializable domain";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN DESERIALIZABLE]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainSerializable(currentSession);
        System.out.println("[OK]");
    }
}
