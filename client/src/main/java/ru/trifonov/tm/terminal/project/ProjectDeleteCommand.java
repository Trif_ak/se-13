package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ProjectDeleteCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT DELETE]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project you want delete");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
//        bootstrap.getTaskEndpoint().deleteAllTaskOfProject(currentSession, id);
        bootstrap.getProjectEndpoint().deleteProject(currentSession, id);
        System.out.println("[OK]");
    }
}
