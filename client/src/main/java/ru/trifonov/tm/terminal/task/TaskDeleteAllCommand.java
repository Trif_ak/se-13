package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class TaskDeleteAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL TASKS]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = bootstrap.getTerminalService().getInCommand();
        bootstrap.getTaskEndpoint().deleteAllTaskOfProject(currentSession, projectId);
        System.out.println("[OK]");
    }

}
