package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Project;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ProjectGetCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT GET SELECT]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        @NotNull final Project project = bootstrap.getProjectEndpoint().getProject(currentSession, id);
        System.out.print("  NAME PROJECT " + project.getName());
        System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
        System.out.println("  ID PROJECT " + project.getId());
        System.out.println("[OK]");
    }

}
