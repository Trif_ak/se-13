package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ProjectInsertCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT INSERT]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter name");
        @Nullable final String name = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter description");
        @Nullable final String description = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        @Nullable final String endDate = bootstrap.getTerminalService().getInCommand();
        bootstrap.getProjectEndpoint().insertProject(currentSession, name, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
