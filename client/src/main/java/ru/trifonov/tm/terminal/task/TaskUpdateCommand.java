package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task you want to update");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new name");
        @Nullable final String name = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new projectId");
        @Nullable final String projectId = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new description");
        @Nullable final String description = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = bootstrap.getTerminalService().getInCommand();
        bootstrap.getTaskEndpoint().updateTask(currentSession, name, id, projectId, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
