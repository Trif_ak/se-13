package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.User;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserGetCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET USER]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @Nullable final User user = bootstrap.getUserEndpoint().getUser(currentSession);
        System.out.print("  LOGIN USER " + user.getLogin());
        System.out.print("  PASSWORD USER " + user.getPasswordHash());
        System.out.println("  ID USER " + user.getId());
        System.out.println("[OK]");
    }
}
