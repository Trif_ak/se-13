package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.User;
import ru.trifonov.tm.terminal.AbstractCommand;

import java.util.Collection;

public final class UserGetAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL USERS]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @Nullable final Collection<User> inputList = bootstrap.getUserEndpoint().getAllUser(currentSession);
        for (@NotNull final User user : inputList) {
            System.out.print("  LOGIN USER " + user.getLogin());
            System.out.print("  PASSWORD USER " + user.getPasswordHash());
            System.out.println("  ID USER " + user.getId());
        }
        System.out.println("[OK]");
    }
}
